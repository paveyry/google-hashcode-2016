class Point:
    x = 0
    y = 0

    def dump(self):
        print(vars(self))

    def __init__(self, x, y):
        self.x = x
        self.y = y

class DropPoint:
    location = None
    # int
    stocks = None

    def dump(self):
        print(self.stocks)
        self.location.dump()

class Data:
    rows = 0
    cols = 0
    drones = 0
    turns = 0
    dronePayload = 0
    # int
    products = []
    # DropPoint
    warehouses = []
    # DropPoint
    orders = []

    def parse(self):
        data = list(map(int, input().split()))
        self.rows = data[0]
        self.cols = data[1]
        self.drones = data[2]
        self.turns = data[3]
        self.dronePayload = data[4]

        # skip number of items
        input()
        self.products = list(map(int, input().split()))

        # warehouses
        n = int(input())
        for _ in range(n):
            data = list(map(int, input().split()))
            dp = DropPoint()
            dp.location = Point(0, 0)
            dp.location.x = data[0]
            dp.location.y = data[1]

            dp.stocks = list(map(int, input().split()))

            self.warehouses.append(dp)

        # orders
        n = int(input())
        for _ in range(n):
            data = list(map(int, input().split()))
            dp = DropPoint()
            dp.location = Point(0,0)
            dp.location.x = data[0]
            dp.location.y = data[1]

            # skip number of items
            input()
            dp.stocks = []
            for __ in range(len(self.products)):
                dp.stocks.append(0)
            data = list(map(int, input().split()))
            for p in data:
                dp.stocks[p] += 1
            self.orders.append(dp)

    def dump(self):
        print(vars(self))
        print("Warehouses:")
        for w in self.warehouses:
            w.dump()
        print("Orders:")
        for w in self.orders:
            w.dump()

