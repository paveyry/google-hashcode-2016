#! /usr/bin/env python3

import data
from command import *

data = data.Data()
data.parse()


# Return a list with one warehouse if enough quantity, a list of many otherwise
def getWarehouseWith(product, quantity):
    possibles = []
    for i in range(0, len(data.warehouses)):
        wh = data.warehouses[i]
        if wh.stocks[product] >= quantity:
            return [i]
        else:
            possibles.append(i)
    return possibles


def run():
    commands = []
    nbProducts = len(data.products)
    droneID = 0
    for orderID in range(0, len(data.orders)):
        order = data.orders[orderID]
        for productID in range(0, nbProducts):
            nbNeededByOrder = order.stocks[productID]
            if nbNeededByOrder > 0:     # For each product asked
                warehouses = getWarehouseWith(productID, nbNeededByOrder)
                for wh in warehouses:
                    nbAvailable = data.warehouses[wh].stocks[productID]
                    nbAbleToCarry = int(data.dronePayload / data.products[productID])
                    nbProductID = min(nbNeededByOrder, min(nbAvailable, nbAbleToCarry))
                    commands.append(Command(droneID, 'L', wh, productID, nbProductID, 0))
                    data.warehouses[wh].stocks[productID] -= nbProductID
                    commands.append(Command(droneID, 'D', orderID, productID, nbProductID, 0))
                    droneID = (droneID + 1) % data.drones
                    order.stocks[productID] -= nbProductID
    return commands



def main():
    serialize(checkTime(run(), data), data)


if __name__ == "__main__":
    main()
