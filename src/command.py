#! /usr/bin/env python3

import sys
from data import *
from tools import *

class Command:
    drone = 0
    commandType = 'A' # L for Load, U for Unload, D for deliver, W for wait
    target = 0 # useless if wait
    product = 0 # useless if wait
    productQty = 0 # useless if wait
    waitTime = 0 # useless if not wait

    def __init__(self, drone, commandType, target, product, productQty, waitTime):
        self.drone = drone
        self.commandType = commandType
        self.target = target
        self.product = product
        self.productQty = productQty
        self.waitTime = waitTime

class Drone:
    id = 0
    pos = Point(0, 0)
    weightCapacity = 0

    def __init__(self, id, pos, capacity):
        self.id = id
        self.pos = pos
        self.weightCapacity = capacity


def checkTime(commands, data):
    totaltime = 0
    cleanedcommands = []
    drones = []
    dronetimes = []
    for i in range(0, data.drones):
        dronetimes.append(0)
        drones.append(Drone(i, data.warehouses[0].location, data.dronePayload))

    for c in commands:
        if c.commandType == 'W':
            dronetimes[c.drone] += c.waitTime
        else:
            if c.commandType == 'D':
                dronetimes[c.drone] += 1 + dist(drones[c.drone].pos, data.orders[c.target].location)
                drones[c.drone].pos = data.orders[c.target].location
            else:
                dronetimes[c.drone] += 1 + dist(drones[c.drone].pos, data.warehouses[c.target].location)
                drones[c.drone].pos = data.warehouses[c.target].location
        if max(dronetimes) < data.turns:
            cleanedcommands.append(c)
        else:
            return cleanedcommands
    return cleanedcommands


def serialize(commands, data):
    sys.stdout.write(str(len(commands)) + '\n')
    for c in commands:
        sys.stdout.write(str(c.drone) + ' ')
        sys.stdout.write(str(c.commandType) + ' ')
        if c.commandType == 'W':
            sys.stdout.write(str(c.waitTime) + ' ')
        else:
            sys.stdout.write(str(c.target) + ' ')
            sys.stdout.write(str(c.product) + ' ')
            sys.stdout.write(str(c.productQty) + ' ')
        sys.stdout.write('\n')